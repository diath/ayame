pub static IRCD_NAME: &str = "ayame";
pub static IRCD_VERSION: &str = "0.1.0";
pub static IRCD_REPOSITORY: &str = "https://github.com/diath/ayame";
pub static IRCD_CONFIG: &str = "config.toml";
pub static IRCD_MOTD: &str = "motd.txt";
